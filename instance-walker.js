/* eslint-disable no-console */
"use strict";

const Promise = require("bluebird");
const subDays = require("date-fns/sub_days");
const startOfDay = require("date-fns/start_of_day");
const format = require("date-fns/format");
const collectResults = require("./collect-results");

class InstanceWalker {
  constructor(instance, since, team, evaluators) {
    this.instance = instance;
    this.since = since;
    this.team = team;
    this.evaluators = evaluators;
    this.gitLabAfter = format(startOfDay(subDays(this.since, 1)), "YYYY-MM-DD");

    this.projectIdSet = new Set();
  }

  async walk() {
    await Promise.map(this.team, username => this.iterateEventsForUser(username), {
      concurrency: 5
    });

    await this.iterateMergeRequestsForProjects();
    await this.iterateIssuesForProjects();
  }

  async iterateEventsForUser(username) {
    let url = `${this.instance}/api/v4/users/${username}/events?after=${this.gitLabAfter}&order_by=created_at&sort=asc`;

    let events;
    try {
      events = await collectResults(url);
    } catch (e) {
      // Ignore users who don't exist on a given instance
      console.log(`# error: ${e.message}`);
      return;
    }

    for (var event of events) {
      if (!event.project_id) continue;

      // Keep track of the things merge requests each user has commented on
      this.projectIdSet.add(event.project_id);

      this.evaluators.forEach(evaluator => evaluator.visitEventForUser(username, event));
    }
  }

  async iterateMergeRequestsForProjects() {
    this.teamAuthoredMergedMergeRequests = [];

    await Promise.map(
      this.projectIdSet.values(),
      async projectId => {
        try {
          let mergeRequests = await collectResults(
            `${this.instance}/api/v4/projects/${projectId}/merge_requests?updated_after=${this.gitLabAfter}&order_by=created_at&sort=asc`
          );

          for (let mergeRequest of mergeRequests) {
            this.evaluators.forEach(evaluator => evaluator.visitMergeRequest(mergeRequest));
          }
        } catch (e) {
          console.error("Unable to fetch merge requests:", e);
          return [];
        }
      },
      {
        concurrency: 1
      }
    );
  }

  async iterateIssuesForProjects() {
    await Promise.map(
      this.projectIdSet.values(),
      async projectId => {
        try {
          let issues = await collectResults(
            `${this.instance}/api/v4/projects/${projectId}/issues?updated_after=${this.gitLabAfter}&order_by=created_at&sort=asc`
          );

          for (let issue of issues) {
            this.evaluators.forEach(evaluator => evaluator.visitIssue(issue));
          }
        } catch (e) {
          console.error("Unable to fetch issues:", e);
          return [];
        }
      },
      {
        concurrency: 5
      }
    );
  }
}

module.exports = InstanceWalker;
